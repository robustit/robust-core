<?php
namespace Robust\Core\UI;

use Robust\Core\UI\Core\BaseUI;


/**
 * Class Dashboard
 * @package Robust\Core\UI
 */
class Notification extends BaseUI
{

    /**
     * @var string
     */
    public $route_name = 'notifications';


}
