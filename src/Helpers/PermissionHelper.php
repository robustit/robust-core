<?php

namespace Robust\Core\Helpers;

use Doctrine\DBAL\Schema\Schema;
use Illuminate\Support\Facades\DB;
use Robust\Admin\Models\Permission;
use Robust\Admin\Models\Role;

/**
 * Class PermissionHelper
 * @package Robust\Core\Helpers
 */
class PermissionHelper
{

    /**
     * @return array
     */
    public function get_all_permissions()
    {
        $all_permissions = [];
        foreach (CoreHelper::names() as $key => $value) {
            $all_permissions[$value] = config("{$key}.permissions.actions");
        }
        //additional permissions
        if(!is_null(config("website.permissions.actions"))){
            $all_permissions['Website'] = config("website.permissions.actions");
        }

        return $all_permissions;

    }

    /**
     * @return array
     */
    public function get_all_permissions_db()
    {
        return $this->get_all_permissions();
    }

    /**c
     * @param $role
     * @param $permission_name
     * @return bool
     */
    public function hasPermission($role, $permission_name)
    {
        if (!isset($role->id)) {
            return false;
        }
        $permissions = Role::find($role->id)->permissions->pluck('id', 'name');
        $permissions = isset($permissions) ? $permissions->toArray() : [];

        return isset($permissions[$permission_name]) ? true : false;
    }

}
