<?php
namespace Robust\Core\Helpers\Currencies;

class NRS extends BaseCurrency
{
    /**
     * @var string
     */
    protected $prefix = 'Rs.';
    /**
     * @var string
     */
    protected $postfix = 'NRS';
}