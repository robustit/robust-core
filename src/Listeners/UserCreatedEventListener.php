<?php
namespace Robust\Core\Listeners;

use Robust\Core\Models\Dashboard;
use Illuminate\Support\Facades\Log;
use Robust\Core\Events\UserCreatedEvent;
use Robust\Core\Notifications\RegistrationNotification;

/**
 * Class UserCreatedEventListener
 * @package Robust\Core\Listeners
 */
class UserCreatedEventListener
{
    /**
     * @param UserCreatedEvent $event
     */
    public function handle(UserCreatedEvent $event)
    {

        //Add a Dashboard for the user
        Dashboard::create([
            'name' => "{$event->user->first_name} Dashboard",
            'slug' => str_slug("{$event->user->first_name} Dashboard"),
            'description' => 'Main Dashboard',
            'is_default' => true,
            'user_id' => $event->user->id
        ]);

        try {
            $event->user->notify(new RegistrationNotification($event->user));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

    }
}
