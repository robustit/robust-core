<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |-------------------------------------y-------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */
    [
        'display_name' => 'General',
        'slug' => 'general-setting',
    ],
    [
        'display_name' => 'Email',
        'slug' => 'email-setting',
    ],
    [
        'display_name' => 'Application',
        'slug' => 'app-setting',
    ],
    [
        'display_name' => 'Contact',
        'slug' => 'contact-setting',
    ],
    [
        'display_name' => 'G-Analytics',
        'slug' => 'ga-analytics',
    ],
    [
        'display_name' => 'FB-Analytics',
        'slug' => 'fb-analytics',
    ],
    [
        'display_name' => 'Maintenance Mode',
        'slug' => 'maintenance-mode',
    ],
    [
        'display_name' => 'Mobile App',
        'slug' => 'mobile-app',
    ],
    [
        'display_name'=>'Mobile SMS',
        'slug' => 'sms-setting',
    ],
    [
        'display_name'=>'Social Links',
        'slug' => 'link-setting',
    ]
];
