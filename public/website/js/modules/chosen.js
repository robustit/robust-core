;
(function ($, FRW, window, document, undefined) {
    'use strict';
    $(document).ready(function ($) {
        
        $(".chosenSelect").chosen({no_results_text: "Oops, nothing found!"});
    });
}(jQuery, FRW, window, document));
