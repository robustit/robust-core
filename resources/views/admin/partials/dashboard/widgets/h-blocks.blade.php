<div class="infobox infobox-green infobox-small infobox-dark">
    <div class="infobox-progress">
        <div class="easy-pie-chart percentage" data-percent="61" data-size="39">
            <span class="percent">61</span>%
        </div>
    </div>

    <div class="infobox-data">
        <div class="infobox-content">Task</div>
        <div class="infobox-content">Completion</div>
    </div>
</div>

<div class="infobox infobox-blue infobox-small infobox-dark">
    <div class="infobox-chart">
        <span class="sparkline" data-values="3,4,2,3,4,4,2,2"></span>
    </div>

    <div class="infobox-data">
        <div class="infobox-content">Earnings</div>
        <div class="infobox-content">$32,000</div>
    </div>
</div>

<div class="infobox infobox-grey infobox-small infobox-dark">
    <div class="infobox-icon">
        <i class="ace-icon fa fa-download"></i>
    </div>

    <div class="infobox-data">
        <div class="infobox-content">Downloads</div>
        <div class="infobox-content">1,205</div>
    </div>
</div>